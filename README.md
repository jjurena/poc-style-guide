# Koala42 Style Guide

## Obsah
- Dokumentace (jak psát style guide)
- Obecné
    - Naming
    - Linting
    - Prettier
    - importy (no duplicit, ...)
- Typescript
    - Obecné (let/const, ...)
    - Generika (jak psát a proč)
- React
    - Obecné (naming, functin/class component, )
    - hooky (základní využití, custom hooky)
- Node.js
- Code Review
    - Vytvoření PR (jak vytvořit PR a požádat o CR)
    - Provedení CR (na co si dát pozor)
